/**
 * Created by dexior on 19.05.15.
 */
var nodemailer = require('nodemailer');
var jade = require('jade');
var models = require('../models/index');
var mailConfig = require('../config/mail.json');

function sendConfirmation(user) {
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: mailConfig.user,
            pass: mailConfig.password
        }
    });

    var mailOptions = {
        from: "ePrzychodnia <eprzychodnia24@gmail.com>",
        to: user.email,
        subject: "Rejestracja zakończona",
        text: "Witaj! Twoje konto w ePrzychodni zostało właśnie utworzone i czeka na akceptację administratora. Zrelaksuj się i spokojnie czekaj. \n Zespół ePrzychodnia",
        html: "<strong>Witaj!</strong> <br> Twoje konto w ePrzychodni zostało właśnie utworzone i czeka na akceptację administratora. Zrelaksuj się i spokojnie czekaj. \n Zespół ePrzychodnia"
        };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            throw error;
        }
    });
}

module.exports = { sendConfirmation: sendConfirmation };
