var LocalStrategy = require('passport-local').Strategy;
var models = require('../models');
var crypto = require('crypto');
var i18n = require('i18n');

var User = models.User;
var Role = models.Role;

module.exports = function (passport) {

    passport.use(new LocalStrategy(
        function (username, password, done) {
            User.find({
                where: {username: username}
            }).then(function (user) {
                if (user) {
                    crypto.pbkdf2(password, user.salt, 4096, 512, 'sha256', function (err, key) {
                        if (err)
                            done(err);

                        if (key.toString('hex') === user.password) {
                            done(null, user);
                        } else {
                            done(null, false);
                        }
                    });
                } else {
                    done(null, false);
                }
            })
        }
    ));

    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function (id, done) {
        User.find({
            include: [{model: Role, as: "roles"}],
            where: {id: id}
        }).then(function (user) {
            done(null, user);
        });
    });

};