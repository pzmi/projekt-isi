var path = require('path');

module.exports = {
    // setup some locales - other locales default to en silently
    locales: ['pl'],

    defaultLocale: 'pl',
// sets a custom cookie name to parse locale settings from
    cookie: 'i18n',
// where to store json files - defaults to './locales'
    directory: path.join(__dirname, '/locales')
};