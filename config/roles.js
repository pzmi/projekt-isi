module.exports = function (roles) {
    roles.failureHandler = function (req, res, action) {
        req.session.returnTo = req.baseUrl;
        res.redirect('/login');
    };

    roles.use(function (req, action) {
        if (req.isAuthenticated()) {
            if (action === 'logged') {
                return true;
            } else {
                return req.user.roles.some(function (currentValue, index, array) {
                    return currentValue.name === action;
                });
            }
        } else {
            return false;
        }
    });
};