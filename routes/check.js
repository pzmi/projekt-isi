/**
 * Created by dexior on 29.03.15.
 */
var express = require('express');
var router = express.Router();
var models = require('../models');
var crypto = require('crypto');

var User = models.User;
/* GET users listing. */
router.get('/', function (req, res, next) {
    User.find({
        where: {username: req.query.username}
    }).then(function (user) {
        if (user) {
            crypto.pbkdf2(req.query.password, user.salt, 4096, 512, 'sha256', function (err, key) {
                if (err)
                    throw err;

                if (key.toString('hex') === user.password) {
                    res.send("Hello " + user.username);

                } else {
                    res.send("wrong password!");
                }
            });
        } else {
            res.send("user " + req.query.username + " not found");
        }
    });


});

module.exports = router;
