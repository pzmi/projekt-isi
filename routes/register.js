/**
 * Created by dexior on 29.03.15.
 */
var express = require('express');
var models = require('../models');
var uuid = require('node-uuid');
var crypto = require('crypto');
var router = express.Router();
var userUtils = require('./users/utils');
var emailUtils = require('../utils/email');

var User = models.User;

var validate = userUtils.validate;

router.get('/', function (req, res, next) {
    res.render('register');
});

router.post('/', function (req, res, next) {
    var body = req.body;

    var error = validate(req);

    if (body.repeat !== body.password) {
        error = true;
        req.flash('error_messages', req.__('Password no match'));
    }

    if (!error) {
        var username = body.username;
        var name = body.name;
        var lastname = body.lastname;
        var date_of_birth = new Date(body.date_of_birth);
        var email = body.email;
        var phone_number = body.phone_number;

        var salt = uuid.v4();
        crypto.pbkdf2(body.password, salt, 4096, 512, 'sha256', function (err, key) {
            if (err)
                throw err;

            User.create(
                {
                    username: username,
                    password: key.toString('hex'),
                    salt: salt,
                    name: name,
                    lastname: lastname,
                    date_of_birth: date_of_birth,
                    email: email,
                    phone_number: phone_number
                }
            ).then(function(user) {
                    emailUtils.sendConfirmation(user);
                });
        });

        req.flash('success_messages', req.__("User saved"));
        res.redirect('/');
    } else {
        res.redirect('back');
    }
});

module.exports = router;
