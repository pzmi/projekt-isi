var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    if (typeof req.logout != 'undefined') {
        req.logout();
    }
    res.redirect('/');
});


module.exports = router;
