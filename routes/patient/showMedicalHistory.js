/**
 * Created by ferdek on 24.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

MedicalHistory = models.MedicalHistory;
Appointment = models.Appointment;

router.get('/', function(req, res, next){
    MedicalHistory.findAll({
        where: {
            user_id: req.user.id
        },
        attributes: ['id', 'diagnosis'],
        include:[{
            model: Appointment,
            as: 'appointment',
            attributes: ['start_hour']
        }]
    }).then(function (histories){
        res.render('patient/showMedicalHistory',{
            title: req.__('Medical histories'),
            histories: histories
        });
    });
});

router.post('/', function(req, res, next){

});

module.exports = router;