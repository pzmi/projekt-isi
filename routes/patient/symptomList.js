/**
 * Created by ferdek on 19.05.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

var Symptom = models.Symptom;

router.get('/', function (req, res, next) {
    var user_id = req.user.id;
    var symptomsPromise = Symptom.findAll({
        where: {
            user_id: user_id
        }
    }).then(function(symptoms){
        console.log(JSON.stringify(symptoms));
        symptoms.sort(function (a, b){
            return parseFloat(b.date.getTime()) - parseFloat(a.date.getTime())
        });
        res.render('patient/symptomList', {title: 'Lista objawów', symptoms: symptoms});
    });

});

module.exports = router;