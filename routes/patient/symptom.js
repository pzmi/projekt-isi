/**
 * Created by ferdek on 19.05.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

var Symptom = models.Symptom;

router.get('/', function (req, res, next) {
    res.render('patient/symptom', {title: 'Dodaj objaw'});;
});

router.post('/', function(req, res, next){
    var desc = req.body.description;
    var user_id = req.user.id;
    var symptom = Symptom.create({
        desc: desc,
        date: new Date(),
        user_id: user_id
    }).then(function(symptom){
        req.flash('success_messages', '"' + desc + '" ' + res.__('Saved successfully'));
        res.redirect('/'); //TODO zmienic na panel usera(chyba)
    });
});

module.exports = router;
