/**
 * Created by ferdek on 19.05.15.
 */

var express = require('express');
var router = express.Router();

router.use('/symptom', require('./symptom'));
router.use('/symptomlist', require('./symptomList'));
router.use('/showmedicalhistory', require('./showMedicalHistory'));
router.use('/historydetails', require('./historyDetails'));
router.use('/showbills', require('./showBills'));

router.get('/', function(req, res, next){
    res.send('patient/index');
});



module.exports = router;