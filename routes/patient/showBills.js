/**
 * Created by ferdek on 24.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

User = models.User;
Bill = models.Bill;
MedicalHistory = models.MedicalHistory;
Appointment = models.Appointment;

router.get('/', function(req, res, next){
    Bill.findAll({
        include:[{
            model: User,
            as: 'user',

            where:{
                id: req.user.id
            },
            attributes: ['id']
        },
        {
            model: MedicalHistory,
            as: 'medicalHistory',
            attributes: ['diagnosis'],
            include: [{
                model: Appointment,
                as: 'appointment',
                attributes: ['start_hour']
            }]
        }]
    }).then(function(bills){
        console.log(JSON.stringify(bills, null, 2));
        res.render('patient/showBills', {
            title: req.__('Bill list'),
            bills: bills
        });
    });
});

router.post('/', function(req, res, next){

});

module.exports = router;