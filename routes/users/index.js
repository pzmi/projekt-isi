var express = require('express');
var router = express.Router();
var models = require('../../models/index');
var uuid = require('node-uuid');
var crypto = require('crypto');
var sequelize = require('sequelize');
var utils = require('./utils');

var User = models.User;
var Role = models.Role;
var validate = utils.validate;
/* GET users listing. */
router.route('/')
    .get(show)
    .post(function (req, res, next) {
        var error = validate(req);

        if (!error) {
            var body = req.body;
            var username = body.username;
            var password = body.password;
            var name = body.name;
            var lastname = body.lastname;
            var dateOfBirth = body.date_of_birth;
            var email = body.email;
            var phoneNumber = body.phone_number;
            var role = body.role;

            User.find({
                where: {
                    $or: {
                        username: {
                            $like: username
                        },
                        email: {
                            $like: email
                        }
                    }
                }
            }).then(function (user) {
                if (user !== null) {
                    req.flash('error_messages', req.__("User exists"));
                } else {
                    req.flash('success_messages', req.__("User saved"));
                    var salt = uuid.v4();
                    crypto.pbkdf2(password, salt, 4096, 512, 'sha256', function (err, key) {
                        if (err)
                            throw err;

                        User.create({
                            username: username,
                            salt: salt,
                            password: key.toString('hex'),
                            name: name,
                            lastname: lastname,
                            date_of_birth: new Date(dateOfBirth),
                            email: email,
                            phone_number: phoneNumber
                        }).then(function (user) {
                            if (req.userIs('admin')) {
                                if (!Array.isArray(role)) {
                                    Role.find({where: {name: role}})
                                        .then(function (role) {
                                            user.setRoles(role);
                                        });
                                } else {
                                    for (var i = 0; i < role.length; i++) {
                                        Role.find({where: {name: role[i]}})
                                            .then(function (role) {
                                                user.setRoles(role);
                                            });
                                    }
                                }
                            }
                        });
                    });
                }

                res.redirect('back');
            });
        }
    })
    .put(update);


router.get('/new', function (req, res, next) {
    Role.findAll().then(function (roles) {
        res.render('users/new', {method: 'post', action: '/users', roles: roles});
    });
});

router.get('/:id', function(req, res, next) {
    var id = req.params.id;
    User.find({ include: [{model: Role, as: "roles"}],  where: { id: id } }).then(function (user){
        Role.findAll().then(function (roles) {
            res.render('users/edit', {method: 'post', action: '/users/' + id, roles: roles, user: user})
        });
    });
});

router.post('/:id', update);

module.exports = router;

function show(req, res, next) {
    User.findAll().then(function (users) {
        res.render('users/index', {title: 'Użytkownicy', users: users});
    });
}

function update(req, res, next) {
    var body = req.body;

    var error = validate(req);

    if (!error) {
        User.findById(body.user_id).then(function (user) {
            user.username = body.username;
            user.name = body.name;
            user.lastname = body.lastname;
            user.date_of_birth = new Date(body.date_of_birth);
            user.email = body.email;
            user.phone_number = body.phone_number;
            var role = body.role;
            if (body.password !== user.password) {
                var salt = uuid.v4();
                crypto.pbkdf2(body.password, salt, 4096, 512, 'sha256', function (err, key) {
                    if (err)
                        throw err;

                    user.salt = salt;
                    user.password = key.toString('hex');

                    user.save().then(function (user) {
                        if (req.userIs('admin')) {
                            if (!Array.isArray(role)) {
                                Role.find({where: {name: role}})
                                    .then(function (role) {
                                        user.setRoles(role);
                                    });
                            } else {
                                for (var i = 0; i < role.length; i++) {
                                    Role.find({where: {name: role[i]}})
                                        .then(function (role) {
                                            user.setRoles(role);
                                        });
                                }
                            }
                        }
                    });

                });
            }
        });

        req.flash('success_messages', req.__("User saved"));
    }

    res.redirect("/users");
}