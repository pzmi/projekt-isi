module.exports = {
    validate: validate
};

function validate(req) {
    var body = req.body;
    var username = body.username;
    var password = body.password;
    var name = body.name;
    var lastname = body.lastname;
    var dateOfBirth = body.date_of_birth;
    var email = body.email;
    var phoneNumber = body.phone_number;

    var error = false;
    if (username.length < 5) {
        req.flash('error_messages', req.__("Username required"));
        error = true;
    }
    if (password.length < 8) {
        req.flash('error_messages', req.__("Password required"));
        error = true;
    }
    if (name.length < 1) {
        req.flash('error_messages', req.__("Name required"));
        error = true;
    }
    if (lastname.length < 1) {
        req.flash('error_messages', req.__("Lastname required"));
        error = true;
    }
    if (dateOfBirth.length < 1) {
        req.flash('error_messages', req.__("Date of birth required"));
        error = true;
    }
    if (email.length < 1) {
        req.flash('error_messages', req.__("Email required"));
        error = true;
    }
    if (phoneNumber.length < 1) {
        req.flash('error_messages', req.__("Phone number required"));
        error = true;
    }

    return error;
}