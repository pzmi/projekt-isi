/**
 * Created by ferdek on 29.05.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

var User = models.User;
var Role = models.Role;
var Appointment = models.Appointment;

router.get('/', show);

router.post('/', function(req, res, next){
    var doctorId = parseInt(req.body.doctor_id);
    var patientId = parseInt(req.body.patient_id);
    var date = new Date(req.body.start_date);
    var end_date = new Date(date).setMinutes(date.getMinutes()+15);
    Appointment.create({
        start_hour: date,
        end_hour: end_date
    }).then(function(appointment){
        User.find({
            where:{
                id: doctorId
            }
        }).then(function(doctor){
            User.find({
                where:{
                    id: patientId
                }
            }).then(function(patient){
                appointment.setUsers([doctor, patient]).then(function(){
                    req.flash('success_messages', res.__('Saved successfully'));
                    show(req, res, next);
                });
            });
        });
    });


});


function show(req, res, next){
    User.findAll({
        include: [{
            model : Role,
            as: 'roles',
            where: {name: "doctor"}
        }]
    }).then(function(doctors){
        User.findAll({
            include: [{
                model: Role,
                as: 'roles',
                where: {name: "patient"}
            }]
        }).then(function(patients){
            res.render('receptionist/addAppointment', {
                title: req.__('Add appointment'),
                patients: patients,
                doctors: doctors
            });
        });
    });
}


module.exports = router;