/**
 * Created by ferdek on 24.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

Appointment = models.Appointment;
User = models.User;
Role = models.Role;


router.get('/:id', function(req, res, next) {
    var id = parseInt(req.params.id);
    Appointment.find({
        where:{
            id: id
        },
        include:[{
            model: User,
            as: 'users',
            include:[{
                model: Role,
                as: 'roles',
                where:{
                    name: 'patient'
                }
            }],
            attributes: ['name', 'lastname']
        }]
    }).then(function(appointment){
        User.findAll({
            include: [{
                model: Role,
                as: 'roles',
                where:{
                    name: 'doctor'
                }
            }]
        }).then(function(doctors){
            console.log(JSON.stringify(appointment, null, 2));
            res.render('receptionist/editAppointment', {
                title: req.__('Edit appointment'),
                appointment: appointment,
                doctors: doctors
            })
        });
    });
});

router.post('/:id', function(req, res, next){
    Appointment.find({
        where:{
            id: parseInt(req.body.appointment_id)
        }
    }).then(function(appointment){
        appointment.start_hour = new Date(req.body.start_date);
        appointment.save().then(function(){
            req.flash('success_messages', req.__('Saved successfully'));
            res.redirect('../showappointments');
        });
    });
});

module.exports = router;