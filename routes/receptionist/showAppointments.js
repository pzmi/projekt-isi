/**
 * Created by ferdek on 20.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

var User = models.User;
var Role = models.Role;
var Appointment = models.Appointment;

router.get('/', function(req, res, next){
    Appointment.findAll({
        include: [{
            model: User,
            as: 'users',
            include: [{
                model: Role,
                as: 'roles'
            }],
            attributes: ['name', 'lastname']

        }],
        attributes: ['id', 'start_hour'],
        order: ['id']
    }).then(function(appointments){
        var doctors = [];
        var patients = [];
        appointments.forEach(function(appointment){
            if (appointment.users[0].roles[0].name === 'patient') {
                patients[appointment.id] = appointment.users[0];
            }
            if (appointment.users[0].roles[0].name === 'doctor'){
                doctors[appointment.id] = appointment.users[0];
            }
            if (appointment.users[1].roles[0].name === 'patient') {
                patients[appointment.id] = appointment.users[1];
            }
            if (appointment.users[1].roles[0].name === 'doctor'){
                doctors[appointment.id] = appointment.users[1];
            }

        });
        res.render('receptionist/showAppointments', {
            title: res.__('Appointments'),
            appointments: appointments,
            patients: patients,
            doctors: doctors
        });
    });
});

router.post('/', function(req, res, next){

});

module.exports = router;