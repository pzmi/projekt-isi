/**
 * Created by ferdek on 29.05.15.
 */

/**
 * Created by ferdek on 19.05.15.
 */

var express = require('express');
var router = express.Router();

router.use('/addappointment', require('./addAppointment'));
router.use('/showappointments', require('./showAppointments'));
router.use('/editappointment', require('./editAppointment'));

router.get('/', function(req, res, next){
    res.render('index', {title: 'ePrzychodnia/receptionist'});
});

router.post('/', function(req, res, next){

});


module.exports = router;