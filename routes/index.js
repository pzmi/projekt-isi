var express = require('express');
var router = express.Router();

module.exports = function (passport, roles) {

    router.use('/admin', require('./admin'));
    router.use('/users', roles.is('admin'), require('./users'));
    router.use('/register', require('./register'));
    router.use('/login', require('./login')(passport));
    router.use('/logout', require('./logout'));
    router.use('/patient', require('./patient'));
    router.use('/receptionist', require('./receptionist'));
    router.use('/doctor', require('./doctor'));
    /* GET home page. */
    router.get('/', function (req, res, next) {
        res.render('index', {title: 'ePrzychodnia'});
    });

    return router;
};


