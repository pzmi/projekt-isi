/**
 * Created by futerzak on 21.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

var Referral = models.Referral;

router.get('/', function(req, res, next){
    MedicalHistory.findAll().then(function(medicalHistory){

        res.render('admin/referraladd', {title: "Dodaj skierowanie", medicalHistory: medicalHistory});
    });
});

router.post('/', function(req, res, next){
    var medicalHistoryId = parseInt(req.body.medicalHistoryId);
    var desc = req.body.description;
    var referral = Referral.create({
        description: desc,
        medicalHistory: medicalHistoryId
    }).then(function(symptom){
        req.flash('success_messages' + "skierowanie (?)" + __('Saved successfully'));
        res.redirect('admin/referralslist');
    });
});

module.exports = router;