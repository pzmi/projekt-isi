/**
 * Created by futerzak on 19.05.15.
 */
var express = require('express');
var router = express.Router();


router.use('/prescriptionslist', require('./prescriptionslist'));
router.use('/prescriptionadd', require('./prescriptionadd'));


router.use('/needlist', require('./needlist'));
router.use('/needadd', require('./needadd'));

router.use('/referralslist', require('./referralslist'));
router.use('/referraladd', require('./referraladd'));


router.get('/', function (req, res, next) {
        res.render('admin/index', {title: 'ePrzychodnia - panel admina'});
    }
);
module.exports = router;
