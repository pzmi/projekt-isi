/**
 * Created by futerzak on 13.06.15.
 */
var express = require('express');
var router = express.Router();
var models = require('../../models');

var Need = models.Need;

router.get('/', function(req, res, next) {
    Need.findAll({include: {model: User, as: "user"}}).then(function(need){
        res.render('admin/needlist', {title: "Zapotrzebowanie", need: need});
    })
});

module.exports = router;