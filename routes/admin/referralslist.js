/**
 * Created by futerzak on 21.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

var Referral = models.Referral;

router.get('/', function(req, res, next){
    Referral.findAll().then(function(referrals){
        res.render('admin/referralslist', {title: "Lista skierowań", referrals: referrals});
    });
});
module.exports = router;