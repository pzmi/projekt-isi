/**
 * Created by futerzak on 19.05.15.
 */
var express = require('express');
var router = express.Router();
var models = require('../../models');

var Prescription = models.Prescription;
var User = models.User;
var Role = models.Role;

router
    .get('/', function(req, res, next) {
        //res.render('admin/prescriptions', {title: "ePrzychodnia prescription"});

        Prescription.findAll({
            include: [
                {
                    model: User, as: "user", include: [
                    {model: Role, as: "roles", where: {name: "patient"}}
                ]
                }]
        }).then(function (prescriptions) {
            console.log(prescriptions);

            User.findAll({include: [{model: Role, as: "roles", where: {name: "patient"}}]}).then(function (users) {
                // console.log(users);
                res.render('admin/prescriptionslist', {
                    title: 'Recepty - ePrzychodnia',
                    users: users,
                    prescription: prescriptions

                });

            });
            //});

        });
    });
router
    .post('/', function (req, res, next) {
        var patient = parseInt(req.body.patient);
        Prescription.findAll({
            include: [
                {
                    model: User, as: "user", where: {id: patient}, include: [
                    {model: Role, as: "roles", where: {name: "patient"}}
                ]
                }
            ]
        }).then(function (prescriptions) {
            User.findAll({include: [{model: Role, as: "roles", where: {name: "patient"}}]}).then(function (users) {
                res.render('admin/prescriptionslist', {
                    title: 'Recepty - ePrzychodnia',
                    users: users,
                    prescription: prescriptions,
                    user_id: req.body.patient
                });
            });
        });
});

module.exports = router;

