/**
 * Created by futerzak on 13.06.15.
 */
var express = require('express');
var router = express.Router();
var models = require('../../models');

var Need = models.Need;

router
    .get('/', function(req, res, next) {
        res.render('admin/needadd',{title: "Dodaj zapotrzebowanie"});
    })
    .post('/', function(req, res, next) {
        Need.create({
            room_number: parseInt(req.body.room_number),
            desc: req.body.need_description,
            user_id: parseInt(req.user.id),
            status: "false"
        });
        res.render('admin/needadd',{title: "Dodaj zapotrzebowanie"});
    });

module.exports = router;