/**
 * Created by futerzak on 13.06.15.
 */
var express = require('express');
var router = express.Router();
var models = require('../../models');

var Prescription = models.Prescription;
var User = models.User;
var Role = models.Role;

router
    .get('/', function(req, res, next) {
        User.findAll({include: [{model: Role, as: "roles", where: {name: "patient"}}]})
            .then(function (users) {
                console.log(users);
                res.render('admin/prescriptionadd', {
                    title: 'Recepty - ePrzychodnia',
                    users: users
                });
            })
    });
router
    .post('/', function(req, res, next){
        Prescription.create({
            user_id: parseInt(req.body.patient),
            medications: req.body.medications
        });

        User.findAll({include: [{model: Role, as: "roles", where: {name: "patient"}}]})
            .then(function (users) {
                res.render('admin/prescriptionadd',{
                    title: 'Recepty - ePrzychodnia',
                    users: users
             });
        })
    });
module.exports = router;
