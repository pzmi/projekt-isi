var express = require('express');
var router = express.Router();

module.exports = function (passport) {
    router.route('/')
        .get(function (req, res, next) {
            res.render('login');
        })

        .post(function (req, res, next) {
            passport.authenticate('local', function (err, user, info) {
                if (err) {
                    return next(err);
                }

                if (!user) {
                    req.flash('error_messages', res.__("Wrong user password"));
                    return res.redirect('back');
                }

                req.login(user, function (err) {
                    if (err) {
                        return next(err);
                    }
                    var returnTo = '/';
                    if ('returnTo' in req.session) {
                        returnTo = req.session.returnTo;
                    }
                    return res.redirect(returnTo);
                });
            })(req, res, next);
        });

    return router;
};
