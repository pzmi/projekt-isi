/**
 * Created by dexior on 07.04.15.
 */
var express = require('express');
var models = require('../models');
var router = express.Router();

var User = models.User;
var Role = models.Role;


router.get('/', function (req, res, next) {
    User.findAll({include: [Role]}).then(function (users) {
        Role.find({
            where: {name: 'admin'}
        }).then(function (admin) {
            for (var i = 0; i < users.length; i++) {
                users[i].addRole(admin);
                users[i].save();
            }
            res.sendStatus(200);
        });
    });

});


module.exports = router;
