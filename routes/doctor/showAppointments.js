/**
 * Created by ferdek on 24.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

var User = models.User;
var Appointment = models.Appointment;

router.get('/', function(req, res, next){
    Appointment.findAll({
        attributes: ['start_hour'],
        include: [{
            model: User,
            as: 'users',
            where:{
                id: req.user.id
            }
        }]
    }).then(function(appointments){
        console.log(JSON.stringify(appointments, null, 2));
        res.render('doctor/showAppointments', {
            title: req.__('Appointments'),
            appointments: appointments
        });
    });
});

router.post('/', function(req, res, next){

});

module.exports = router;