/**
 * Created by ferdek on 13.06.15.
 */


var express = require('express');
var router = express.Router();
var models = require('../../models');

var User = models.User;
var Role = models.Role;
var Appointment = models.Appointment;
var Prescription = models.Prescription;
var Referral = models.Referral;
var MedicalHistory = models.MedicalHistory;
var Bill = models.Bill;

router.get('/', function(req, res, next) {
    User.findAll({
        include: [{
            model: Role,
            as: 'roles',
            where: {name: "patient"}
        }]
    }).then(function (patients) {
        Appointment.findAll({
            include: [{
                model: MedicalHistory,
                as: 'medicalHistory'
            }]
        }).then(function (appointments) {
            res.render('doctor/addMedicalHistory', {
                title: req.__('Add record'),
                patients: patients,
                appointments: appointments
            });
        });
    });
});

router.post('/', function(req, res, next){
    var userId = parseInt(req.body.patient_id);
    MedicalHistory.create({
        user_id: userId,
        appointment_id: parseInt(req.body.appointment_id),
        diagnosis: req.body.diagnosis,
        recommendation: req.body.recommendation
    }).then(function(history){
        if (req.body.prescriptioncheckbox){
            Prescription.create({
                user_id: userId,
                medications: req.body.prescription
            }).then(function(prescription){
                history.setPrescription(prescription);
            });
        }
        if (req.body.referralcheckbox){
            Referral.create({
                user_id: userId,
                description: req.body.referral
            }).then(function(referral){
                history.setReferral(referral);
            });
        }
        if (req.body.billcheckbox){
            Bill.create({
                user_id: userId,
                amount: req.body.bill
            }).then(function(bill){
                bill.setMedicalHistory(history);
            });
        }
    });
    req.flash('success_messages', req.__('Saved successfully'));
    res.redirect('addmedicalhistory');
});

module.exports = router;