/**
 * Created by ferdek on 24.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

ConsultingHours = models.ConsultingHours;
Weekday = models.Weekday;

router.get('/:id', function(req, res, next){
    ConsultingHours.find({
        where:{
            id: req.params.id
        },
        include: [{
            model: Weekday,
            as: 'weekday'
        }]
    }).then(function(consultingHours){
        Weekday.findAll().then(function(weekdays){
            res.render('doctor/editConsultingHours', {
                title: req.__('Edit consulting hours'),
                consultingHours: consultingHours,
                weekdays: weekdays
            });
        });
    });
});

router.post('/:id', function(req, res, next){
    ConsultingHours.find({
        where:{
            id: parseInt(req.body.consultingHours_id)
        },
        include: [{
            model: Weekday,
            as: 'weekday'
        }]
    }).then(function(consultingHours){
        consultingHours.weekday.id = parseInt(req.body.weekday_id);
        consultingHours.start_hour = new Date(req.body.start_date);
        consultingHours.end_hour = new Date(req.body.end_date);
        consultingHours.save().then(function(){
            req.flash('success_messages', req.__('Saved successfully'));
            res.redirect('../showconsultinghours');
        });
    });
});

module.exports = router;