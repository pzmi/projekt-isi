/**
 * Created by ferdek on 29.05.15.
 */

var express = require('express');
var router = express.Router();

router.use('/addmedicalhistory', require('./addMedicalHistory'));
router.use('/showmedicalhistory', require('./showMedicalHistory'));
router.use('/historydetails', require('./historyDetails'));
router.use('/showconsultinghours', require('./showConsultingHours'));
router.use('/addconsultinghours', require('./addConsultingHours'));
router.use('/editconsultinghours', require('./editConsultingHours'));
router.use('/showappointments', require('./showAppointments'));

router.get('/', function(req, res, next){
    res.send('doctor/index');
});


module.exports = router;