/**
 * Created by ferdek on 23.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

User = models.User;
ConsultingHours = models.ConsultingHours;
Weekday = models.Weekday;

router.get('/', function(req, res, next){
    ConsultingHours.findAll({
        include:[{
            model: Weekday,
            as: 'weekday'
        }],
        where:{
            user_id: req.user.id
        }
    }).then(function(consultingHours){
        console.log(JSON.stringify(consultingHours, null, 2));
        res.render('doctor/showConsultingHours', {
            title: req.__('Consulting hours'),
            consultingHours: consultingHours
        });
    });
});

router.post('/', function(req, res, next){

});

module.exports = router;