/**
 * Created by ferdek on 23.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

MedicalHistory = models.MedicalHistory;
Appointment = models.Appointment;
User = models.User;
Bill = models.Bill;
Referral = models.Referral;
Prescription = models.Prescription;

router.get('/:id', function(req, res, next) {
    var id = req.params.id;
    MedicalHistory.find({
        where:{
            id: id
        },
        include: [{
            model: Prescription,
            as: 'prescription'
        },
        {
            model: Appointment,
            as: 'appointment'
        },
        {
            model: User,
            as: 'user',
            attributes: ['name', 'lastname', 'date_of_birth']
        },
        {
            model: Bill,
            as: 'bill'
        },
        {
            model: Referral,
            as: 'referral'
        }]
    }).then(function(history){
        console.log(JSON.stringify(history, null, 2));
        res.render('doctor/historyDetails', {
            title: req.__('Record details'),
            history: history
        })
    });
});


module.exports= router;