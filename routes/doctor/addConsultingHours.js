/**
 * Created by ferdek on 24.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

User = models.User;
ConsultingHours = models.ConsultingHours;
Weekday = models.Weekday;

router.get('/', function(req, res, next){
    Weekday.findAll().then(function(weekdays){
        res.render('doctor/addConsultingHours', {
            title: req.__('Add consulting hours'),
            weekdays: weekdays,
            userId: req.user.id
        });
    });

});

router.post('/', function(req, res, next){
    console.log(req.body.user_id);
    console.log(parseInt(req.body.user_id));
    ConsultingHours.create({
        user_id: parseInt(req.body.user_id),
        weekday_id: parseInt(req.body.weekday_id),
        start_hour: new Date(req.body.start_date),
        end_hour: new Date(req.body.end_date)
    }).then(function(consultingHours){
        req.flash('success_messages', req.__('Saved successfully'));
        res.redirect('addconsultinghours');
    });
});

module.exports = router;