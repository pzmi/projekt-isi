/**
 * Created by ferdek on 13.06.15.
 */

var express = require('express');
var router = express.Router();
var models = require('../../models');

MedicalHistory = models.MedicalHistory;
Appointment = models.Appointment;
User = models.User;
Role = models.Role;

router.get('/', function(req, res, next){
    User.findAll({
        include: [{
            model: Appointment,
            as: 'appointments',
            include: [{
                model: MedicalHistory,
                as: 'medicalHistory'
            }],
            required: true,
            attributes: ['id', 'start_hour']
        },
            {
                model: Role,
                as: 'roles',
                where: {
                    name: 'patient'
                }
            }],
        attributes: ['name', 'lastname']
    }).then(function (users) {
        console.log(JSON.stringify(users, null, 2));
        res.render('doctor/showMedicalHistory', {
            title: req.__('Medical histories'),
            users: users
        });
    });
});

router.post('/', function(req, res, next){

});

module.exports = router;