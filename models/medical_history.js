/**
 * Created by ferdek on 30.04.15.
 */
"use strict";
module.exports = function (sequelize, DataTypes) {
    var MedicalHistory = sequelize.define("MedicalHistory", {
        diagnosis: DataTypes.STRING,
        recommendation: DataTypes.STRING
    }, {
        classMethods: {
            associate: function (models) {
                MedicalHistory.hasOne(models.Bill, {as: 'bill'});
                MedicalHistory.belongsTo(models.Referral, {as: 'referral'});
                MedicalHistory.belongsTo(models.Prescription, {as: 'prescription'});
                MedicalHistory.belongsTo(models.Appointment, {as: 'appointment'});
                MedicalHistory.belongsTo(models.User, {as: 'user'});
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return MedicalHistory;
};

//Task.belongsTo(User, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' })
//User.hasMany(Task, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' })