/**
 * Created by ferdek on 30.04.15.
 */
/**
 * Created by ferdek on 30.04.15.
 */

"use strict";
module.exports = function (sequelize, DataTypes) {
    var Weekday = sequelize.define("Weekday", {
        day: DataTypes.STRING
    }, {
        classMethods: {
            associate: function (models) {
                Weekday.hasMany(models.ConsultingHours, {as: 'consultingHours'});
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return Weekday;
};