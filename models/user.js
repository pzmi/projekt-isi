"use strict";
module.exports = function (sequelize, DataTypes) {
    var User = sequelize.define("User", {
        username: DataTypes.STRING,
        password: DataTypes.TEXT,
        salt: DataTypes.STRING,
        name: DataTypes.STRING,
        lastname: DataTypes.STRING,
        date_of_birth: DataTypes.DATE,
        email: DataTypes.STRING,
        phone_number: DataTypes.STRING
    }, {
        classMethods: {
            associate: function (models) {
                User.belongsToMany(models.Role, {through: 'user_roles', as: 'roles'});
                User.belongsToMany(models.User, {through: 'family_member', as: 'children'});
                User.belongsToMany(models.Appointment, {through: 'user_appointments', as: 'appointments'});
                User.hasMany(models.ConsultingHours, {foreignKey: {allowNull: false}, as: 'consultingHours'});
                User.hasMany(models.MedicalHistory, {foreignKey: {allowNull: false}, as: 'medicalHistories'});
                User.hasMany(models.Prescription, {foreignKey: {allowNull: false}, as: 'prescriptions'});
                User.hasMany(models.Bill, {foreignKey: {allowNull: false}, as: 'bills'});
                User.hasMany(models.Referral, {foreignKey: {allowNull: false}, as: 'referrals'});
                User.hasMany(models.Test, {foreignKey: {allowNull: false}, as: 'tests'});
                User.hasMany(models.Symptom, {foreignKey: {allowNull: false}, as: 'symptoms'});
                User.hasMany(models.Need, {foreignKey: {allowNull: false}, as: 'needs'});
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return User;
};