/**
 * Created by ferdek on 30.04.15.
 */

"use strict";
module.exports = function (sequelize, DataTypes) {
    var ConsultingHours = sequelize.define("ConsultingHours", {
        start_hour: DataTypes.DATE,
        end_hour: DataTypes.DATE
    }, {
        classMethods: {
            associate: function (models) {
                ConsultingHours.belongsTo(models.Weekday, {as: 'weekday'});
                ConsultingHours.belongsTo(models.User, {as: 'user'});
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return ConsultingHours;
};