/**
 * Created by ferdek on 30.04.15.
 */

"use strict";
module.exports = function (sequelize, DataTypes) {
    var Appointment = sequelize.define("Appointment", {
        start_hour: DataTypes.DATE,
        end_hour: DataTypes.DATE
    },
        {
        classMethods: {
            associate: function (models) {
                Appointment.hasOne(models.MedicalHistory, {as: 'medicalHistory'});
                Appointment.belongsToMany(models.User, {through: 'user_appointments', as: 'users'});
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return Appointment;
};