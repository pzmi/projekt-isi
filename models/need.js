/**
 * Created by ferdek on 02.05.15.
 */

"use strict";
module.exports = function (sequelize, DataTypes) {
    var Need = sequelize.define("Need", {
        room_number: DataTypes.INTEGER,
        desc: DataTypes.STRING,
        status: DataTypes.BOOLEAN
    }, {
        classMethods: {
            associate: function (models) {
                Need.belongsTo(models.User, {as: "user"});
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return Need;
};