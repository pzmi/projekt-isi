/**
 * Created by ferdek on 30.04.15.
 */

"use strict";
module.exports = function (sequelize, DataTypes) {
    var Prescription = sequelize.define("Prescription", {
        medications: DataTypes.STRING
    }, {
        classMethods: {
            associate: function (models) {
                Prescription.hasOne(models.MedicalHistory, {as: 'medicalHistory'});
                Prescription.belongsTo(models.User,{as: 'user'});
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return Prescription;
};