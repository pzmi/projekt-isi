/**
 * Created by ferdek on 02.05.15.
 */

"use strict";
module.exports = function (sequelize, DataTypes) {
    var Symptom = sequelize.define("Symptom", {
        desc: DataTypes.STRING,
        date: DataTypes.DATE
    }, {
        classMethods: {
            associate: function (models) {
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return Symptom;
};