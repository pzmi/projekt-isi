/**
 * Created by ferdek on 02.05.15.
 */

"use strict";
module.exports = function (sequelize, DataTypes) {
    var Test = sequelize.define("Test", {
        desc: DataTypes.STRING,
        date: DataTypes.DATE
    }, {
        classMethods: {
            associate: function (models) {
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return Test;
};