/**
 * Created by ferdek on 30.04.15.
 */

"use strict";
module.exports = function (sequelize, DataTypes) {
    var Address = sequelize.define("Address", {
        street: DataTypes.STRING,
        number: DataTypes.STRING,
        postcode: DataTypes.STRING,
        city: DataTypes.STRING
    }, {
        classMethods: {
            associate: function (models) {
                Address.hasOne(models.User);
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return Address;
};