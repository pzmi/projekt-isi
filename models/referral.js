/**
 * Created by ferdek on 30.04.15.
 */

"use strict";
module.exports = function (sequelize, DataTypes) {
    var Referral = sequelize.define("Referral", {
        description: DataTypes.STRING
    }, {
        classMethods: {
            associate: function (models) {
                Referral.hasOne(models.MedicalHistory, {as: 'history'});
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return Referral;
};