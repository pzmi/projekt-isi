/**
 * Created by ferdek on 30.04.15.
 */

"use strict";
module.exports = function (sequelize, DataTypes) {
    var Bill = sequelize.define("Bill", {
        amount: DataTypes.FLOAT
    }, {
        classMethods: {
            associate: function (models) {
                Bill.belongsTo(models.MedicalHistory, {as: 'medicalHistory'});
                Bill.belongsTo(models.User, {as: 'user'});
            }
        },
        underscored: true,
        underscoredAll: true
    });
    return Bill;
};