"use strict";
var Role = require('../models').Role;
module.exports = {
    up: function (migration, DataTypes, done) {
        migration.createTable("Roles", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER
            },
            name: {
                type: DataTypes.STRING
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE
            }
        }).then(function () {
            // FIXME: unikalność na role
            Role.findOrCreate({where: {name: 'admin'}});
            Role.findOrCreate({where: {name: 'staff'}});
            Role.findOrCreate({where: {name: 'doctor'}});
            Role.findOrCreate({where: {name: 'patient'}});
        }).done(done);
    },
    down: function (migration, DataTypes, done) {
        migration.dropTable("Roles").done(done);
    }
};