INSERT INTO roles (name, created_at, updated_at) VALUES ('admin', NOW(), NOW()), ('doctor', NOW(), NOW()), ('staff', NOW(), NOW()), ('patient', NOW(), NOW());

insert into user_roles (user_id, role_id, created_at, updated_at) values (1, 4, NOW(), NOW()), (2, 4, NOW(), NOW()), (3, 4, NOW(), NOW()), (4, 3, NOW(), NOW()), (5, 3, NOW(), NOW()), (6, 3, NOW(), NOW()), (7, 2, NOW(), NOW()), (8, 2, NOW(), NOW()), (9, 2, NOW(), NOW());

UPDATE "users" SET name = 'Shay', lastname = 'Newton', date_of_birth = '2015-09-05', email = 'ac@euplacerat.org' WHERE id = 1;
UPDATE "users" SET name = 'Kaitlin', lastname = 'Hendrix', date_of_birth = '2015-11-24', email = 'rutrum.urna@liberolacus.com' WHERE id = 2;
UPDATE "users" SET name = 'Hayley', lastname = 'Bradshaw', date_of_birth = '2015-08-22', email = 'conubia.nostra@arcu.edu' WHERE id = 3;
UPDATE "users" SET name = 'Dale', lastname = 'Green', date_of_birth = '2015-08-11', email = 'nec@liberoProinmi.net' WHERE id = 4;
UPDATE "users" SET name = 'Deborah', lastname = 'Keith', date_of_birth = '2015-06-05', email = 'lobortis@pedeCrasvulputate.org' WHERE id = 5;
UPDATE "users" SET name = 'Robert', lastname = 'Schultz', date_of_birth = '2015-06-03', email = 'varius@nequeseddictum.net' WHERE id = 6;
UPDATE "users" SET name = 'Philip', lastname = 'Neal', date_of_birth = '2014-11-05', email = 'Fusce@iaculis.org' WHERE id = 7;
UPDATE "users" SET name = 'Noelle', lastname = 'Wilkinson', date_of_birth = '2015-03-22', email = 'Nulla@fermentumrisusat.edu' WHERE id = 8;
UPDATE "users" SET name = 'Reuben', lastname = 'Crosby', date_of_birth = '2015-05-21', email = 'sodales@eueratsemper.net' WHERE id = 9;
UPDATE "users" SET name = 'Wayne', lastname = 'Shepard', date_of_birth = '2014-12-27', email = 'ipsum@necurna.com' WHERE id = 10;
UPDATE "users" SET name = 'Arsenio', lastname = 'Strong', date_of_birth = '2015-07-28', email = 'congue.a@ligulaAliquam.ca' WHERE id = 11;
UPDATE "users" SET name = 'Carlos', lastname = 'Emerson', date_of_birth = '2014-06-20', email = 'diam.Sed@liberodui.org' WHERE id = 12;
UPDATE "users" SET name = 'Ifeoma', lastname = 'Brock', date_of_birth = '2014-12-05', email = 'vel@quisurna.co.uk' WHERE id = 13;
UPDATE "users" SET name = 'Gabriel', lastname = 'Huffman', date_of_birth = '2016-05-07', email = 'Vivamus.molestie@nullaInteger.net' WHERE id = 14;

INSERT INTO "appointments" (start_hour,end_hour,updated_at,created_at) VALUES ('2016-06-07 08:13:05','2015-12-07 19:28:42','2015-05-17 01:09:25','2014-08-24 21:18:57'),('2016-06-08 16:10:22','2015-07-06 08:21:58','2014-07-12 19:11:28','2015-02-17 22:08:29'),('2016-06-01 06:44:40','2015-04-10 13:13:12','2015-03-31 12:44:10','2014-11-05 15:14:52'),('2016-06-13 04:09:23','2014-11-24 13:46:16','2015-03-09 12:00:19','2015-04-29 23:14:36'),('2016-06-06 09:48:51','2014-07-04 10:29:45','2015-03-28 13:37:11','2014-09-20 00:10:51'),('2016-06-08 10:58:17','2016-03-11 09:39:38','2014-11-29 13:13:22','2016-01-05 03:59:23'),('2016-06-10 21:10:11','2015-12-17 02:31:29','2014-07-30 19:36:24','2015-05-30 14:34:06'),('2016-06-15 07:43:50','2015-06-04 19:34:17','2014-11-26 16:17:48','2015-08-08 14:23:00'),('2016-06-12 16:10:02','2015-12-25 09:21:15','2015-05-28 03:02:59','2015-06-29 13:37:06'),('2016-06-13 05:50:25','2015-09-14 03:03:48','2015-06-03 06:29:04','2015-09-17 20:08:21');

INSERT INTO "user_appointments" (user_id,appointment_id,updated_at,created_at) VALUES (2,1,'2014-11-20 07:50:50','2014-11-28 12:47:50'),(3,2,'2014-12-31 10:26:21','2016-05-26 20:46:40'),(3,3,'2015-04-14 22:22:43','2016-04-24 05:34:58'),(1,4,'2015-06-14 03:31:01','2016-06-14 10:28:07'),(3,5,'2014-10-05 16:13:49','2016-05-25 16:41:16'),(1,6,'2014-12-02 15:43:23','2015-06-11 22:41:17'),(2,7,'2015-04-08 01:22:50','2014-08-09 03:52:24'),(2,8,'2015-03-22 08:56:23','2014-07-04 16:11:34'),(2,9,'2014-10-29 03:16:17','2016-05-01 07:41:22'),(2,10,'2015-01-28 15:45:36','2016-02-07 14:00:18');

INSERT INTO "user_appointments" (user_id,appointment_id,updated_at,created_at) VALUES (7,1,'2015-04-17 23:41:35','2016-03-01 05:04:49'),(9,2,'2015-03-17 02:04:23','2015-02-05 02:07:38'),(7,3,'2014-10-29 22:46:32','2014-11-28 03:31:11'),(7,4,'2015-03-14 21:16:18','2016-05-18 03:41:32'),(9,5,'2015-05-24 14:04:09','2014-10-11 02:21:00'),(8,6,'2014-08-07 17:32:44','2014-12-27 16:09:06'),(8,7,'2014-07-12 20:30:26','2014-08-31 13:24:53'),(8,8,'2014-07-10 07:32:34','2016-05-05 10:58:23'),(8,9,'2014-07-23 10:09:49','2015-08-26 04:42:35'),(9,10,'2014-08-30 06:26:18','2014-08-02 05:10:35');

INSERT INTO weekdays (id, day, created_at, updated_at)
VALUES (1, 'Poniedziałek', NOW(), NOW()), (2, 'Wtorek', NOW(), NOW()), (3, 'Środa', NOW(), NOW()), (4, 'Czwartek', NOW(), NOW()), (5, 'Piątek', NOW(), NOW()), (6, 'Sobota', NOW(), NOW()), (7, 'Niedziela', NOW(), NOW());