var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var logger = require('./config/log');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');
var ConnectRoles = require('connect-roles');
var i18n = require('i18n');

var app = express();

require('./config/passport')(passport);

var roles = new ConnectRoles();
require('./config/roles')(roles);

var routes = require('./routes')(passport, roles);

i18n.configure(require('./config/i18n'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname, 'public/favicon.ico')));
app.use(morgan('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser('sssssssssssh'));
app.use(session({
    cookie: {maxAge: 60000},
    secret: 'sssssssssssh',
    saveUninitialized: false,
    resave: true
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(roles.middleware());
app.use(function (req, res, next) {
    res.locals.thatUserCan = testRole;
    res.locals.thatUserIs = testRole;
    next();
});
app.use(i18n.init);
app.use(function (req, res, next) {
    res.locals.success_messages = req.flash('success_messages');
    res.locals.error_messages = req.flash('error_messages');
    res.locals.user = req.user;
    res.locals.nav = req.path;
    next();
});
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
      logger.warn(err);
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

function testRole(user, action) {
    if (typeof user === 'undefined') {
        return false;
    }

    return user.roles.some(function (currentValue, index, array) {
        return currentValue.name === action;
    });
}

module.exports = app;
