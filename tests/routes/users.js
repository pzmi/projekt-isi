var app = require("../../app.js");
var request = require('supertest');
var assert = require('assert');
var utils = require("../../routes/users/utils");
var validate = utils.validate;
var models = require("../../models");
var User = models.User;
var Role = models.Role;

describe('Testing user data verifications', function(){
    var goodUser = {
        username: "goodUserName",
        password: "goodLongPassword",
        name: "Any",
        lastname: "Any",
        surname: "Any",
        date_of_birth: "1990-01-01",
        email: "a@a.pl",
        phone_number: "2135789"
    };

    var badUser = {
        username: "u",
        password: "1",
        name: "",
        lastname: "",
        surname: "",
        date_of_birth: "",
        email: "a.pl",
        phone_number: ""
    };

    it("should pass good user validation", function (done) {
        var req = {
            body: goodUser
        };

        assert.equal(false, validate(req));
        done();
    });

    it("should not pass bad user validation", function (done) {
        var req = {
            body: badUser,
            flash: function(a, b) {},
            __: function(a) {}
        };

        assert.equal(true, validate(req));
        done();
    })
});

describe('Testing logged access to users', function () {
    var testadmin;
    var agent = request.agent(app);

    app.get('/testlogin', function (req,res) {
        req.login(testadmin);
    });

    before(function (done){
        testadmin = User.build(
            {
                username: "testadmin",
                password: "testadmin",
                salt: "testadmin",
                name: "testadmin",
                lastname: "testadmin",
                date_of_birth: new Date("1990-01-01"),
                email: "testadmin@a.pl",
                phone_number: "11111111"
            }
        );

        var role = Role.build(
            {
                name: "admin"
            }
        );

        testadmin.roles = [role];

        request(app)
            .post('/login')
            .send({
                username: "sysadmin",
                password: "sysadmin12345"
            })
            .end(function (err, res) {
                if (err) {
                    if (err.status != 302)
                        return done(err);
                }

                agent.saveCookies(res);

                done();
            });
    });

    after(function () {
        testadmin.destroy();
    });

    it('should return 200', function (done) {
        var req;

        req = request(app)
            .get('/users')
            .expect(200);

        // attach cookies
        agent.attachCookies(req);

        // do your reqeust
        req.end(done);
    });

});

describe('Testing not logged access to users', function () {
    it('should redirect to login', function (done) {
        var req;

        req = request(app)
            .get('/users')
            .expect(302);

        // do your reqeust
        req.end(done);
    });

});